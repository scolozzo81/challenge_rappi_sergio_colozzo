# Ejercicio Técnico
## Objetivo
Plantear una solución al ejercicio técnico propuesto basado en una aplicación Android

### Arquitectura
Se utilizó la arquitectura MVVM


### Observaciones:
Motivado al factor tiempo los request no tienen paginado sin embargo puede ser implementada esta funcionalidad agregando la librería de Paging 3  
cambiando los Recyclerview por PagingDataAdapter con las variaciones que este requiere como lo es el DiffUtil.ItemCallback, luego crear una clase que herede de PagingSource  
 que se encargara de hacer la carga del paginado realizando el request correspondiente al API con la pagina de paginado que corresponde para que esta sea entregada al viewmodel.  
 Luego en el ViewModel este se encargará de enviarle la información a la vista, en mi caso que use live data, con un método de la librería flow llamado cachedIn.

En el caso de la vista principal de las películas mostrada por categoría, puede ser agregado un botón que se dirija a un nuevo fragment donde se pueda observar con paginado todas  
las películas según la categoría seleccionada en un listado vertical similar al realizado en el SearchFragment.
