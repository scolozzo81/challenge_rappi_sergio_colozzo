package com.ae.mobilechallenge.util

import com.ae.mobilechallenge.data.local.entity.MovieEntity
import com.ae.mobilechallenge.dto.Movie

/**
 * Converter from movies
 * to movie entity
 */
fun Movie.toMovieEntity(type: String) = MovieEntity(
    this.id,
    this.overview,
    this.originalLanguage,
    this.originalTitle,
    this.video,
    this.title,
    this.posterPath,
    this.backdropPath,
    this.releaseDate,
    this.voteAverage,
    this.popularity,
    this.adult,
    this.voteCount,
    movieType = type
)

/**
 * Converter from movies entity
 * to movie
 */
fun MovieEntity.toMovie(): Movie = Movie(
    this.id,
    this.overview,
    this.originalLanguage,
    this.originalTitle,
    this.video,
    this.title,
    this.posterPath,
    this.backdropPath,
    this.releaseDate,
    this.voteAverage,
    this.popularity,
    this.adult,
    this.voteCount,
    this.movieType
)

/**
 * Converter from List of Movies Entity
 * to List of Movie
 */
fun List<MovieEntity>.toMovie(): List<Movie> {
    return this.map { it.toMovie() }
}
