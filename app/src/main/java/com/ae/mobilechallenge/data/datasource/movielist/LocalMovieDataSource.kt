package com.ae.mobilechallenge.data.datasource.movielist

import com.ae.mobilechallenge.data.local.entity.MovieEntity
import com.ae.mobilechallenge.dto.Movie

interface LocalMovieDataSource {
    /**
     * Get popular movies from DB
     * @return list of movies saved
     */
    suspend fun getPopularMovies(): List<Movie>

    /**
     * Get Top Rated movies from DB
     * @return list of movies saved
     */
    suspend fun getTopRatedMovies(): List<Movie>

    /**
     * Save movies in DB
     * @param movie to save
     */
    suspend fun saveMovie(movie: MovieEntity)
}
