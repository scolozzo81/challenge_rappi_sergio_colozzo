package com.ae.mobilechallenge.data.datasource.movielist

import com.ae.mobilechallenge.data.local.dao.MovieDao
import com.ae.mobilechallenge.data.local.entity.MovieEntity
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.dto.MovieType
import com.ae.mobilechallenge.util.toMovie
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalMovieDataSourceImpl @Inject constructor(
    private val movieDao: MovieDao
) : LocalMovieDataSource {
    override suspend fun getPopularMovies(): List<Movie> {
        val movies = movieDao.getMoviesByCategory(MovieType.POPULAR.type)
        return movies.toMovie()
    }

    override suspend fun getTopRatedMovies(): List<Movie> {
        val movies = movieDao.getMoviesByCategory(MovieType.TOP_RATED.type)
        return movies.toMovie()
    }

    override suspend fun saveMovie(movie: MovieEntity) {
        movieDao.insertMovie(movie)
    }
}
