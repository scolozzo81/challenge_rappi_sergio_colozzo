package com.ae.mobilechallenge.data.datasource.searchmovie

import com.ae.mobilechallenge.dto.Movie

interface LocalMovieSearchDataSource {
    /**
     * Get top all movies from DB
     * @return response with movies and paging data
     */
    suspend fun getAllMovies(): List<Movie>
}
