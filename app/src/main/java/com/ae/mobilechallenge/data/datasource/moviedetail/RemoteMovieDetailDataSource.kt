package com.ae.mobilechallenge.data.datasource.moviedetail

import com.ae.mobilechallenge.dto.DataVideoResponse
import com.ae.mobilechallenge.dto.MovieResult

interface RemoteMovieDetailDataSource {
    /**
     * Get data about video movie from API
     * @return data
     */
    suspend fun getDataMovie(movieId: String): DataVideoResponse
}
