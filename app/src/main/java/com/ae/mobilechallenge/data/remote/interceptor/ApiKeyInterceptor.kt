package com.ae.mobilechallenge.data.remote.interceptor

import com.ae.mobilechallenge.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

/**
 * API KEY interceptor
 *
 * Add API KEY to every request
 */
class ApiKeyInterceptor @Inject constructor() : Interceptor {

    companion object {
        private const val API_KEY: String = "api_key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(API_KEY, BuildConfig.API_KEY)
            .build()

        val requestBuilder: Request.Builder = original.newBuilder()
            .url(url)

        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}
