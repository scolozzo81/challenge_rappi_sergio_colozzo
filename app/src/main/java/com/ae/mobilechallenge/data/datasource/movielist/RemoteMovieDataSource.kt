package com.ae.mobilechallenge.data.datasource.movielist

import com.ae.mobilechallenge.dto.Response

interface RemoteMovieDataSource {
    /**
     * Get popular movies from API
     * @return response with movies and paging data
     */
    suspend fun getPopularMovies(): Response

    /**
     * Get top rated movies from API
     * @return response with movies and paging data
     */
    suspend fun getTopRatedMovies(): Response
}
