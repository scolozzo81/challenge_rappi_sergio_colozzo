package com.ae.mobilechallenge.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ae.mobilechallenge.data.local.dao.MovieDao
import com.ae.mobilechallenge.data.local.entity.MovieEntity

@Database(entities = [MovieEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}
