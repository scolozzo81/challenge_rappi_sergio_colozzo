package com.ae.mobilechallenge.data.remote.service

import com.ae.mobilechallenge.dto.DataVideoResponse
import com.ae.mobilechallenge.dto.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit Movies Service
 */
interface MoviesService {

    /**
     * Method to get popular movies from API REST
     *
     * @return response with popular movies
     */
    @GET("movie/popular")
    suspend fun getPopularMovies(): Response

    /**
     * Method to get the top rated movies from API REST
     *
     * @return response with top rated movie
     */
    @GET("movie/top_rated")
    suspend fun getTopRated(): Response

    /**
     * Method to get data of any movie from API REST
     * @param movieId id of movie to find
     * @return response data about video in youtube about movie
     */
    @GET("movie/{movie_id}/videos")
    suspend fun getDataMovie(
        @Path("movie_id") movieId: String
    ): DataVideoResponse

    /**
     * Method to get Movies by name
     * @param query to find movie
     * @param language of movies to find
     * @return response with top rated movie
     */
    @GET("search/movie")
    suspend fun getMovieByName(
        @Query("query") query: String,
        @Query("language") language: String,
    ): Response
}
