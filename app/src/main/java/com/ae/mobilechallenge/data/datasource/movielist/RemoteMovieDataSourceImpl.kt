package com.ae.mobilechallenge.data.datasource.movielist

import com.ae.mobilechallenge.data.remote.service.MoviesService
import com.ae.mobilechallenge.dto.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteMovieDataSourceImpl @Inject constructor(
    private val service: MoviesService
) : RemoteMovieDataSource {
    override suspend fun getPopularMovies(): Response {
        return service.getPopularMovies()
    }

    override suspend fun getTopRatedMovies(): Response {
        return service.getTopRated()
    }
}
