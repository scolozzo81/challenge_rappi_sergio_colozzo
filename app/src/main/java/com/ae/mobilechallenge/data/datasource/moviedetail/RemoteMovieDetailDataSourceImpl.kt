package com.ae.mobilechallenge.data.datasource.moviedetail

import com.ae.mobilechallenge.data.remote.service.MoviesService
import com.ae.mobilechallenge.dto.DataVideoResponse
import com.ae.mobilechallenge.dto.MovieResult
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteMovieDetailDataSourceImpl @Inject constructor(
    private val service: MoviesService
) : RemoteMovieDetailDataSource {
    override suspend fun getDataMovie(movieId: String): DataVideoResponse {
        return service.getDataMovie(movieId)
    }
}
