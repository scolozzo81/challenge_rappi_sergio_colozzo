package com.ae.mobilechallenge.data.datasource.searchmovie

import com.ae.mobilechallenge.data.remote.service.MoviesService
import com.ae.mobilechallenge.dto.MovieLanguage
import com.ae.mobilechallenge.dto.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteMovieSearchDataSourceImpl @Inject constructor(
    private val service: MoviesService
) : RemoteMovieSearchDataSource {

    override suspend fun getTMovieByName(query: String): Response {
        return service.getMovieByName(query, MovieLanguage.ENGLISH.type)
    }
}
