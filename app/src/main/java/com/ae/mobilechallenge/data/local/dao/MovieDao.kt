package com.ae.mobilechallenge.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ae.mobilechallenge.data.local.entity.MovieEntity

@Dao
interface MovieDao {
    /**
     * Method to get data from DB
     *
     * @return results
     */
    @Query("SELECT * FROM movie_entity  ORDER BY created_at DESC")
    suspend fun getAllMovies(): List<MovieEntity>

    /**
     * Method to get data from DB
     *
     * @return results
     */
    @Query("SELECT * FROM movie_entity WHERE movie_type = :type  ORDER BY created_at DESC")
    suspend fun getMoviesByCategory(type: String): List<MovieEntity>

    /**
     * Insert results in DB
     *
     * @param movie
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: MovieEntity)
}
