package com.ae.mobilechallenge.data.datasource.searchmovie

import com.ae.mobilechallenge.dto.Response

interface RemoteMovieSearchDataSource {
    /**
     * Get top rated movies from API
     * @param query word to find movie
     * @return response with movies and paging data
     */
    suspend fun getTMovieByName(query: String): Response
}
