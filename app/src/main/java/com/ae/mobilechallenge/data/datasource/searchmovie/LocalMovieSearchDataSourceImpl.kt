package com.ae.mobilechallenge.data.datasource.searchmovie

import com.ae.mobilechallenge.data.local.dao.MovieDao
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.util.toMovie
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalMovieSearchDataSourceImpl @Inject constructor(
    private val movieDao: MovieDao
) : LocalMovieSearchDataSource {
    override suspend fun getAllMovies(): List<Movie> {
        return movieDao.getAllMovies().toMovie()
    }
}
