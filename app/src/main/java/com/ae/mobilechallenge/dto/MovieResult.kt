package com.ae.mobilechallenge.dto

import com.google.gson.annotations.SerializedName

data class MovieResult(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("key")
    val key: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("site")
    val site: String = "",
)
