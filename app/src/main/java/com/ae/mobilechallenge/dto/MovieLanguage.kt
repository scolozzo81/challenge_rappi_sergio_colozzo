package com.ae.mobilechallenge.dto

/**
 * Handler movies languages
 */
enum class MovieLanguage(val type: String) {
    ENGLISH("en-US"),
}
