package com.ae.mobilechallenge.dto

import com.google.gson.annotations.SerializedName

data class DataVideoResponse(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("results")
    val results: List<MovieResult> = arrayListOf(),
)
