package com.ae.mobilechallenge.dto

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("page")
    val page: Int = 0,
    @SerializedName("total_pages")
    val totalPages: Int = 0,
    @SerializedName("results")
    val movies: List<Movie>,
    @SerializedName("total_results")
    val totalResults: Int = 0
)
