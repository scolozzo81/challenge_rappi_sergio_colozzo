package com.ae.mobilechallenge.dto

/**
 * Handler movies types
 */
enum class MovieType(val type: String) {
    POPULAR("popular"),
    TOP_RATED("top_rated")
}
