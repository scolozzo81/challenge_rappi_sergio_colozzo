package com.ae.mobilechallenge.ui.searchmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.searchmovie.repository.SearchMovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class SearchMovieViewModel @Inject constructor(
    private val repository: SearchMovieRepository
) : ViewModel() {

    fun getMoviesByName(query: String): LiveData<Resource<List<Movie>>> = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            val response = repository.getTMovieByName(query)
            emit(ResponseHandler.handleSuccess(response))
        } catch (e: Exception) {
            emit(ResponseHandler.handleException<List<Movie>>(e))
        }
    }

    fun getLastMoviesDownloaded(): LiveData<Resource<List<Movie>>> = liveData(Dispatchers.IO) {
        try {
            val result = repository.getAllMovies()
            emit(ResponseHandler.handleSuccess(result))
        } catch (e: Exception) {
            emit(ResponseHandler.handleException<List<Movie>>(e))
        }
    }
}
