package com.ae.mobilechallenge.ui.moviedetail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.MovieResult
import com.ae.mobilechallenge.ui.moviedetail.repository.MovieDetailRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val repository: MovieDetailRepository
) : ViewModel() {

    fun getDataMovie(movieId: String): LiveData<Resource<List<MovieResult>>> = liveData {
        try {
            val response = repository.getDataMovie(movieId)
            emit(ResponseHandler.handleSuccess(response))
        } catch (e: Exception) {
            emit(ResponseHandler.handleException<List<MovieResult>>(e))
        }
    }
}
