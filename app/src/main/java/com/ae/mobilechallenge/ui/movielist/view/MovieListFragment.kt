package com.ae.mobilechallenge.ui.movielist.view

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.ae.mobilechallenge.R
import com.ae.mobilechallenge.common.base.BaseFragment
import com.ae.mobilechallenge.common.listener.MoviesListener
import com.ae.mobilechallenge.common.resource.Status
import com.ae.mobilechallenge.databinding.FragmentMovieListBinding
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.movielist.adapter.MovieListAdapter
import com.ae.mobilechallenge.ui.movielist.viewmodel.MovieListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieListFragment : BaseFragment(), MoviesListener {

    private lateinit var binding: FragmentMovieListBinding
    private val viewModel: MovieListViewModel by viewModels()

    @Inject
    lateinit var concatAdapter: ConcatAdapter

    @Inject
    lateinit var popularAdapter: MovieListAdapter

    @Inject
    lateinit var topRatedAdapter: MovieListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieListBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        getMoviesMovies()
    }

    private fun setupView() {
        configureEmptyState(binding.root, binding.mainRv, binding.emptyStateView)
        setupSearchView()
    }

    private fun setupSearchView() {
        binding.searchMovies.setOnClickListener {
            goToSearch()
        }
    }

    private fun getMoviesMovies() {
        viewModel.getMovies().observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        response.data?.let { it ->
                            if (it.first.isEmpty() || it.second.isEmpty()) {
                                showEmptyState()
                            } else {
                                concatAdapter.apply {
                                    addAdapter(
                                        0,
                                        getPopularAdapter(it.first)
                                    )
                                    addAdapter(
                                        1,
                                        getTopRated(it.second)
                                    )
                                }
                                setupConcatAdapter()
                                hideEmptyState()
                            }
                        }
                    }
                    Status.ERROR -> {
                        showEmptyState()
                    }
                }
            }
        )
    }

    private fun getPopularAdapter(movies: List<Movie>): MovieListAdapter {
        return popularAdapter.apply {
            setTitle(requireContext().resources.getString(R.string.items_popular_title))
            setMoviesList(movies)
            setMovieListener(this@MovieListFragment)
        }
    }

    private fun getTopRated(movies: List<Movie>): MovieListAdapter {
        return topRatedAdapter.apply {
            setTitle(requireContext().resources.getString(R.string.items_top_rated_title))
            setMoviesList(movies)
            setMovieListener(this@MovieListFragment)
        }
    }

    private fun setupConcatAdapter() {
        binding.mainRv.apply {
            layoutManager = getLayoutManagerAppointment()
            adapter = concatAdapter
        }
    }

    private fun getLayoutManagerAppointment() = LinearLayoutManager(
        context,
        LinearLayoutManager.VERTICAL,
        false
    )

    override fun goToDetail(movie: Movie) {
        val bundle = bundleOf(MOVIE_PARAM to movie)
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_movieListFragment_to_movieDetailFragment, bundle)
    }

    private fun goToSearch() {
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_movieListFragment_to_searchMoviesFragment)
    }

    override fun onStop() {
        super.onStop()
    }

    companion object {
        private const val MOVIE_PARAM: String = "movie"
    }
}
