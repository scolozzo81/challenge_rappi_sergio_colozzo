package com.ae.mobilechallenge.ui.searchmovie.adapter

import android.annotation.SuppressLint
import android.widget.Filter
import com.ae.mobilechallenge.dto.Movie
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomFilter @Inject constructor() : Filter() {
    private var originalList: List<Movie> = arrayListOf()
    private var filteredList: ArrayList<Movie> = arrayListOf()

    private var adapter: SearchMoviesAdapter? = null

    fun setMovieList(movies: List<Movie>) {
        originalList = movies
    }

    fun setAdapter(newAdapter: SearchMoviesAdapter) {
        adapter = newAdapter
    }

    @SuppressLint("DefaultLocale")
    override fun performFiltering(constraint: CharSequence?): FilterResults {
        filteredList.clear()
        val results = FilterResults()
        constraint?.let {
            if (it.isEmpty()) {
                filteredList.addAll(originalList)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim()
                originalList.forEach {
                    if (it.title.toLowerCase().contains(filterPattern)) {
                        filteredList.add(it)
                    }
                }
            }
        }

        results.values = filteredList
        results.count = filteredList.size
        return results
    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
        adapter?.let {
            it.getMovies().clear()
            if (results != null) {
                try {
                    it.getMovies().addAll(results.values as ArrayList<Movie>)
                    it.notifyDataSetChanged()
                } catch (error: TypeCastException) {
                    Timber.e(error)
                }
            }
        }
    }
}
