package com.ae.mobilechallenge.ui.searchmovie.repository

import com.ae.mobilechallenge.data.datasource.searchmovie.LocalMovieSearchDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.RemoteMovieSearchDataSource
import com.ae.mobilechallenge.dto.Movie
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchMovieRepositoryImpl @Inject constructor(
    private val local: LocalMovieSearchDataSource,
    private val remote: RemoteMovieSearchDataSource
) : SearchMovieRepository {

    override suspend fun getTMovieByName(query: String): List<Movie> {
        return remote.getTMovieByName(query).movies
    }

    override suspend fun getAllMovies(): List<Movie> {
        return local.getAllMovies()
    }
}
