package com.ae.mobilechallenge.ui.searchmovie.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.ae.mobilechallenge.BuildConfig
import com.ae.mobilechallenge.R
import com.ae.mobilechallenge.common.listener.MoviesListener
import com.ae.mobilechallenge.databinding.DetailItemBinding
import com.ae.mobilechallenge.dto.Movie
import com.bumptech.glide.Glide
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchMoviesAdapter @Inject constructor(
    private val customFilter: CustomFilter
) :
    RecyclerView.Adapter<SearchMoviesAdapter.ViewHolder>(), Filterable {
    private var list: ArrayList<Movie> = arrayListOf()
    private var moviesList: ArrayList<Movie> = arrayListOf()
    private var listener: MoviesListener? = null

    fun setMoviesList(movies: List<Movie>) {
        list.addAll(movies)
        moviesList.addAll(movies)
        notifyDataSetChanged()
    }

    fun setMovieListener(moviesListener: MoviesListener) {
        listener = moviesListener
    }

    fun clearAll() {
        moviesList.clear()
        list.clear()
        notifyDataSetChanged()
    }

    fun getMovies(): ArrayList<Movie> {
        return moviesList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.detail_item,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(moviesList[position], holder.itemView.context)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = DetailItemBinding.bind(itemView)

        fun bind(item: Movie, context: Context) {
            val imageURL = BuildConfig.BASE_URL_IMAGES + item.posterPath

            binding.detailItemTitle.text = item.originalTitle
            binding.detailItemOverview.text = item.overview
            onClickItem(item)
            setImage(context, imageURL)
        }

        private fun onClickItem(movie: Movie) {
            binding.detailItemContainer.setOnClickListener {
                listener?.let {
                    it.goToDetail(movie)
                }
            }
        }

        private fun setImage(context: Context, imageURL: String) {
            Glide.with(context)
                .load(imageURL)
                .centerCrop()
                .into(binding.detailItemImage)
        }
    }

    override fun getFilter(): Filter {
        moviesList.clear()
        moviesList.addAll(list)
        customFilter.setAdapter(this)
        customFilter.setMovieList(moviesList)
        return customFilter
    }
}
