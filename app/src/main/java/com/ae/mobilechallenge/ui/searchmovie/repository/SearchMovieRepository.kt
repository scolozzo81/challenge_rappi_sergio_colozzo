package com.ae.mobilechallenge.ui.searchmovie.repository

import com.ae.mobilechallenge.dto.Movie

interface SearchMovieRepository {
    /**
     * Get top rated movies from API
     * @param query word to find movie
     * @return response with movies and paging data
     */
    suspend fun getTMovieByName(query: String): List<Movie>

    /**
     * Get top all movies from DB
     * @return response with movies and paging data
     */
    suspend fun getAllMovies(): List<Movie>
}
