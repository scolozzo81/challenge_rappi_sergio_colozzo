package com.ae.mobilechallenge.ui.moviedetail.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.ae.mobilechallenge.common.base.BaseFragment
import com.ae.mobilechallenge.common.resource.Status
import com.ae.mobilechallenge.databinding.FragmentMovieDetailBinding
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.moviedetail.viewmodel.MovieDetailViewModel
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MovieDetailFragment : BaseFragment() {

    private lateinit var binding: FragmentMovieDetailBinding
    private val viewModel: MovieDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieDetailBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        configureEmptyState(binding.root, binding.informationContainer, binding.emptyStateView)
        getArgsOfDetail()
    }

    private fun getArgsOfDetail() {
        try {
            val movieSelected = arguments?.get(MOVIE_PARAM)
            movieSelected?.let {
                val movie = it as Movie
                showInfoAboutMovie(movie)
                getMoviesMovies(movie.id.toString())
                binding.informationContainer.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            Timber.e(e)
            showEmptyState()
        }
    }

    private fun getMoviesMovies(movieId: String) {
        viewModel.getDataMovie(movieId).observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        response.data?.let { it ->
                            if (it.isEmpty()) {
                                showLocalData()
                            } else {
                                setupYoutubeView(it[0].key)
                                hideEmptyState()
                            }
                        }
                    }
                    Status.ERROR -> {
                        showLocalData()
                    }
                }
            }
        )
    }

    private fun showLocalData() {
        binding.youtubePlayerView.visibility = View.GONE
        binding.informationContainer.visibility = View.VISIBLE
        hideEmptyState()
    }

    private fun setupYoutubeView(key: String) {
        lifecycle.addObserver(binding.youtubePlayerView)
        binding.youtubePlayerView.addYouTubePlayerListener(object :
                AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.loadVideo(key, 0F)
                }
            })
    }

    private fun showInfoAboutMovie(movie: Movie) {
        binding.movieDetailTitle.text = movie.title
        binding.movieDetailOverview.text = movie.overview
    }

    companion object {
        private const val MOVIE_PARAM: String = "movie"
    }
}
