package com.ae.mobilechallenge.ui.searchmovie.view

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ae.mobilechallenge.R
import com.ae.mobilechallenge.common.base.BaseFragment
import com.ae.mobilechallenge.common.listener.MoviesListener
import com.ae.mobilechallenge.common.resource.Status
import com.ae.mobilechallenge.databinding.FragmentSearchBinding
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.searchmovie.adapter.SearchMoviesAdapter
import com.ae.mobilechallenge.ui.searchmovie.viewmodel.SearchMovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchMoviesFragment : BaseFragment(), MoviesListener {

    private lateinit var binding: FragmentSearchBinding
    private val viewModel: SearchMovieViewModel by viewModels()

    @Inject
    lateinit var searchAdapter: SearchMoviesAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchView()
        setupViews()
        setupRv()
        getLastMoviesDownloaded()
    }

    private fun setupViews() {
        configureEmptyState(binding.root, binding.searchRv, binding.emptyStateView)
    }

    private fun setupRv() {
        binding.searchRv.apply {
            adapter = searchAdapter
            layoutManager = provideLayoutManager()
        }
        searchAdapter.setMovieListener(this)
    }

    private fun provideLayoutManager(): LinearLayoutManager {
        return LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
    }

    private fun setupSearchView() {
        binding.searchMovies.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    searchMoviesMovies(it)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    if (it.isEmpty()) {
                        getLastMoviesDownloaded()
                    } else {
                        searchAdapter.filter.filter(newText)
                    }
                }

                return false
            }
        })
    }

    private fun getLastMoviesDownloaded() {
        viewModel.getLastMoviesDownloaded().observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        response.data?.let { result ->
                            if (result.isEmpty()) {
                                notFoundData()
                            } else {
                                showData(result)
                            }
                        }
                    }
                    Status.ERROR -> {
                        showEmptyState()
                    }
                }
            }
        )
    }

    private fun searchMoviesMovies(query: String) {
        viewModel.getMoviesByName(query).observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response.status) {
                    Status.LOADING -> {
                        reloadEmptyState()
                    }
                    Status.SUCCESS -> {
                        response.data?.let { result ->
                            if (result.isEmpty()) {
                                notFoundData()
                            } else {
                                showData(result)
                            }
                        }
                    }
                    Status.ERROR -> {
                        showEmptyState()
                    }
                }
            }
        )
    }

    private fun showData(movies: List<Movie>) {
        hideEmptyState()
        searchAdapter.clearAll()
        searchAdapter.setMoviesList(movies)
    }

    private fun notFoundData() {
        showEmptyState()
        searchAdapter.clearAll()
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.applicationWindowToken, HIDE_KEYBOARD_FLAG)
    }

    override fun goToDetail(movie: Movie) {
        val bundle = bundleOf(MOVIE_PARAM to movie)
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_searchMoviesFragment_to_movieDetailFragment, bundle)
    }

    companion object {
        private const val MOVIE_PARAM: String = "movie"
        private const val HIDE_KEYBOARD_FLAG: Int = 0
    }
}
