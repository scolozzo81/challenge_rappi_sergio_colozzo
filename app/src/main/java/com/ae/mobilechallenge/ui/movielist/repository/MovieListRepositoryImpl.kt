package com.ae.mobilechallenge.ui.movielist.repository

import com.ae.mobilechallenge.data.datasource.movielist.LocalMovieDataSource
import com.ae.mobilechallenge.data.datasource.movielist.RemoteMovieDataSource
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.dto.MovieType
import com.ae.mobilechallenge.util.CheckConnection
import com.ae.mobilechallenge.util.toMovieEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieListRepositoryImpl @Inject constructor(
    private val localDataSource: LocalMovieDataSource,
    private val remoteDataSource: RemoteMovieDataSource,
    private val checkConnection: CheckConnection
) : MovieListRepository {
    override suspend fun getPopularMovies(): List<Movie> {
        return if (checkConnection.connectionIsAvailable()) {
            remoteDataSource.getPopularMovies().movies.forEach {
                localDataSource.saveMovie(it.toMovieEntity(MovieType.POPULAR.type))
            }
            localDataSource.getPopularMovies()
        } else {
            localDataSource.getPopularMovies()
        }
    }

    override suspend fun getTopRatedMovies(): List<Movie> {
        return if (checkConnection.connectionIsAvailable()) {
            remoteDataSource.getTopRatedMovies().movies.forEach {
                localDataSource.saveMovie(it.toMovieEntity(MovieType.TOP_RATED.type))
            }
            localDataSource.getTopRatedMovies()
        } else {
            localDataSource.getTopRatedMovies()
        }
    }
}
