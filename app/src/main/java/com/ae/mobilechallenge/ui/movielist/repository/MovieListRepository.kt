package com.ae.mobilechallenge.ui.movielist.repository

import com.ae.mobilechallenge.dto.Movie

interface MovieListRepository {
    /**
     * Get popular movies from
     * data source
     * @return list of movies by popular category
     */
    suspend fun getPopularMovies(): List<Movie>

    /**
     * Get popular movies from
     * data source
     * @return list of movies by top rated category
     */
    suspend fun getTopRatedMovies(): List<Movie>
}
