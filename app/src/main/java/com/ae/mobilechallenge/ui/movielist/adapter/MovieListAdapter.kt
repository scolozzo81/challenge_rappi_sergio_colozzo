package com.ae.mobilechallenge.ui.movielist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ae.mobilechallenge.R
import com.ae.mobilechallenge.common.listener.MoviesListener
import com.ae.mobilechallenge.databinding.BaseItemBinding
import com.ae.mobilechallenge.dto.Movie
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieListAdapter @Inject constructor() : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private lateinit var title: String
    private lateinit var list: List<Movie>
    private lateinit var listener: MoviesListener

    fun setTitle(text: String) {
        title = text
    }

    fun setMoviesList(movies: List<Movie>) {
        list = movies
    }

    fun setMovieListener(moviesListener: MoviesListener) {
        listener = moviesListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.base_item,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = BaseItemBinding.bind(itemView)

        fun bind() {
            binding.categoryTitle.text = title
            setupRv()
        }

        private fun setupRv() {
            binding.rvItems.apply {
                layoutManager = layoutManager()
                adapter = MovieItemAdapter(list, listener)
            }
        }

        private fun layoutManager(): LinearLayoutManager {
            return LinearLayoutManager(
                binding.rvItems.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }
}
