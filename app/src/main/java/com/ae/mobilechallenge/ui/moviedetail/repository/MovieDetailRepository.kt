package com.ae.mobilechallenge.ui.moviedetail.repository

import com.ae.mobilechallenge.dto.DataVideoResponse
import com.ae.mobilechallenge.dto.MovieResult

interface MovieDetailRepository {
    /**
     * Get data about video movie from API
     * @return data
     */
    suspend fun getDataMovie(movieId: String): List<MovieResult>
}
