package com.ae.mobilechallenge.ui.moviedetail.repository

import com.ae.mobilechallenge.data.datasource.moviedetail.RemoteMovieDetailDataSource
import com.ae.mobilechallenge.dto.MovieResult
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieDetailRepositoryImpl @Inject constructor(
    private val remoteMovieDetailDataSource: RemoteMovieDetailDataSource
) : MovieDetailRepository {

    override suspend fun getDataMovie(movieId: String): List<MovieResult> {
        return remoteMovieDetailDataSource.getDataMovie(movieId).results
    }
}
