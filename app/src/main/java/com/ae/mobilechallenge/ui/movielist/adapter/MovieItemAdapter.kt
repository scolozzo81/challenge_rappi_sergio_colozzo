package com.ae.mobilechallenge.ui.movielist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ae.mobilechallenge.BuildConfig
import com.ae.mobilechallenge.R
import com.ae.mobilechallenge.common.listener.MoviesListener
import com.ae.mobilechallenge.databinding.ItemBinding
import com.ae.mobilechallenge.dto.Movie
import com.bumptech.glide.Glide

class MovieItemAdapter(
    private val list: List<Movie>,
    private val listener: MoviesListener
) : RecyclerView.Adapter<MovieItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position], holder.itemView.context)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemBinding.bind(itemView)

        fun bind(item: Movie, context: Context) {
            val imageURL = BuildConfig.BASE_URL_IMAGES + item.posterPath
            binding.apply {
                setImage(context, imageURL)
            }
            binding.cardContainer.setOnClickListener {
                sendToDetail(item)
            }
        }

        private fun setImage(context: Context, imageURL: String) {
            Glide.with(context)
                .load(imageURL)
                .centerCrop()
                .into(binding.image)
        }

        private fun sendToDetail(movie: Movie) {
            listener.goToDetail(movie)
        }
    }
}
