package com.ae.mobilechallenge.ui.movielist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.movielist.repository.MovieListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class MovieListViewModel @Inject constructor(
    private val repository: MovieListRepository
) : ViewModel() {

    fun getMovies(): LiveData<Resource<Pair<List<Movie>, List<Movie>>>> = liveData(Dispatchers.IO) {
        try {
            val popular = repository.getPopularMovies()
            val topRated = repository.getTopRatedMovies()
            emit(ResponseHandler.handleSuccess(Pair(popular, topRated)))
        } catch (e: Exception) {
            emit(ResponseHandler.handleException<Pair<List<Movie>, List<Movie>>>(e))
        }
    }
}
