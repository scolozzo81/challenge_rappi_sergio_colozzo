package com.ae.mobilechallenge.common.resource

enum class ErrorType {
    HTTP_EXCEPTION,
    GENERAL_ERROR
}
