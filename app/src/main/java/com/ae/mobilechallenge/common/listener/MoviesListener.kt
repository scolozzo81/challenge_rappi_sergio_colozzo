package com.ae.mobilechallenge.common.listener

import com.ae.mobilechallenge.dto.Movie

interface MoviesListener {
    fun goToDetail(movie: Movie)
}
