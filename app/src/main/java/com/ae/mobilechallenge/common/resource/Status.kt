package com.ae.mobilechallenge.common.resource

/**
 * Enum about status
 * of Resource
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}