package com.ae.mobilechallenge.core.di

import com.ae.mobilechallenge.data.remote.service.MoviesService
import com.ae.mobilechallenge.util.CheckConnection
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    /**
     * Provide OverviewService
     */
    @Provides
    fun provideMoviesService(retrofit: Retrofit): MoviesService {
        return retrofit.create(MoviesService::class.java)
    }

    /**
     * Provide
     */
    @Provides
    fun provide(): CheckConnection {
        return CheckConnection()
    }
}
