package com.ae.mobilechallenge.core.di

import com.ae.mobilechallenge.BuildConfig
import com.ae.mobilechallenge.data.remote.interceptor.ApiKeyInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    private const val TIME_TIMEOUT: Long = 30

    /**
     * Provide interceptor of http requests
     * @return HttpLoggingInterceptor
     */
    @Provides
    fun provideNetworkInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    /**
     * Provide interceptor with API KEY
     * @return Interceptor
     */
    @Provides
    fun provideInterceptor(): Interceptor {
        return ApiKeyInterceptor()
    }

    /**
     * Provide Http client with behaviors
     * @return OkHttpClient
     */
    @Provides
    fun provideOkHttpClient(
        interceptor: ApiKeyInterceptor,
        networkInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        val httpBuilder = OkHttpClient.Builder()
            .addNetworkInterceptor(networkInterceptor)
            .connectTimeout(TIME_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIME_TIMEOUT, TimeUnit.SECONDS)
            .followRedirects(true)
            .followSslRedirects(true)
            .addInterceptor(interceptor)
        return httpBuilder.build()
    }

    /**
     * Create interface with retrofit generic class
     * @return service to create
     */
    @Provides
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
    }
}
