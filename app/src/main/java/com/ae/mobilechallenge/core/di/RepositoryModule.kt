package com.ae.mobilechallenge.core.di

import com.ae.mobilechallenge.data.datasource.moviedetail.RemoteMovieDetailDataSource
import com.ae.mobilechallenge.data.datasource.movielist.LocalMovieDataSource
import com.ae.mobilechallenge.data.datasource.movielist.RemoteMovieDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.LocalMovieSearchDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.RemoteMovieSearchDataSource
import com.ae.mobilechallenge.ui.moviedetail.repository.MovieDetailRepository
import com.ae.mobilechallenge.ui.moviedetail.repository.MovieDetailRepositoryImpl
import com.ae.mobilechallenge.ui.movielist.repository.MovieListRepository
import com.ae.mobilechallenge.ui.movielist.repository.MovieListRepositoryImpl
import com.ae.mobilechallenge.ui.searchmovie.repository.SearchMovieRepository
import com.ae.mobilechallenge.ui.searchmovie.repository.SearchMovieRepositoryImpl
import com.ae.mobilechallenge.util.CheckConnection
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
internal object RepositoryModule {

    /**
     * Provide MovieListRepository
     */
    @Provides
    @ViewModelScoped
    fun provideMovieListRepository(
        local: LocalMovieDataSource,
        remote: RemoteMovieDataSource,
        checkConnection: CheckConnection
    ): MovieListRepository {
        return MovieListRepositoryImpl(local, remote, checkConnection)
    }

    /**
     * Provide MovieListRepository
     */
    @Provides
    @ViewModelScoped
    fun provideMovieDetailRepository(
        remote: RemoteMovieDetailDataSource
    ): MovieDetailRepository {
        return MovieDetailRepositoryImpl(remote)
    }

    /**
     * Provide SearchMovieRepository
     */
    @Provides
    @ViewModelScoped
    fun provideSearchMovieRepository(
        local: LocalMovieSearchDataSource,
        remote: RemoteMovieSearchDataSource
    ): SearchMovieRepository {
        return SearchMovieRepositoryImpl(local, remote)
    }
}
