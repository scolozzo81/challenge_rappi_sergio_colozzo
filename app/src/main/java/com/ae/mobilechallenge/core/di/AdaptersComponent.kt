package com.ae.mobilechallenge.core.di

import androidx.recyclerview.widget.ConcatAdapter
import com.ae.mobilechallenge.ui.movielist.adapter.MovieListAdapter
import com.ae.mobilechallenge.ui.searchmovie.adapter.CustomFilter
import com.ae.mobilechallenge.ui.searchmovie.adapter.SearchMoviesAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AdaptersComponent {

    /**
     * Provide concat adapter
     */
    @Provides
    fun provideConcatAdapter(): ConcatAdapter {
        return ConcatAdapter()
    }

    /**
     * Provide MovieListAdapter
     */
    @Provides
    fun provideMovieListAdapter(): MovieListAdapter {
        return MovieListAdapter()
    }

    /**
     * Provide CustomFilter
     */
    @Provides
    fun provideCustomFilter(): CustomFilter {
        return CustomFilter()
    }

    /**
     * Provide SearchMoviesAdapter
     */
    @Provides
    fun provideSearchMoviesAdapter(customFilter: CustomFilter): SearchMoviesAdapter {
        return SearchMoviesAdapter(customFilter)
    }
}
