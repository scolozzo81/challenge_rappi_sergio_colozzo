package com.ae.mobilechallenge.core.di

import android.content.Context
import androidx.room.Room
import com.ae.mobilechallenge.data.datasource.moviedetail.RemoteMovieDetailDataSource
import com.ae.mobilechallenge.data.datasource.moviedetail.RemoteMovieDetailDataSourceImpl
import com.ae.mobilechallenge.data.datasource.movielist.LocalMovieDataSource
import com.ae.mobilechallenge.data.datasource.movielist.LocalMovieDataSourceImpl
import com.ae.mobilechallenge.data.datasource.movielist.RemoteMovieDataSource
import com.ae.mobilechallenge.data.datasource.movielist.RemoteMovieDataSourceImpl
import com.ae.mobilechallenge.data.datasource.searchmovie.LocalMovieSearchDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.LocalMovieSearchDataSourceImpl
import com.ae.mobilechallenge.data.datasource.searchmovie.RemoteMovieSearchDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.RemoteMovieSearchDataSourceImpl
import com.ae.mobilechallenge.data.local.AppDatabase
import com.ae.mobilechallenge.data.local.dao.MovieDao
import com.ae.mobilechallenge.data.remote.service.MoviesService
import com.ae.mobilechallenge.util.CheckConnection
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {
    private const val DB_NAME: String = "movies_db"

    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            DB_NAME
        ).build()
    }

    /**
     * Provide LocalMovieDataSource
     */
    @Provides
    fun provideMovieDao(appDatabase: AppDatabase): MovieDao {
        return appDatabase.movieDao()
    }

    /**
     * Provide LocalMovieDataSource
     */
    @Provides
    fun provideLocalMovieDataSource(movieDao: MovieDao): LocalMovieDataSource {
        return LocalMovieDataSourceImpl(movieDao)
    }

    /**
     * Provide RemoteMovieDataSource
     */
    @Provides
    fun provideRemoteMovieDetailDataSource(service: MoviesService): RemoteMovieDetailDataSource {
        return RemoteMovieDetailDataSourceImpl(service)
    }

    /**
     * Provide RemoteMovieDataSource
     */
    @Provides
    fun provideRemoteMovieDataSource(service: MoviesService): RemoteMovieDataSource {
        return RemoteMovieDataSourceImpl(service)
    }

    /**
     * Provide RemoteMovieSearchDataSource
     */
    @Provides
    fun provideRemoteMovieSearchDataSource(service: MoviesService): RemoteMovieSearchDataSource {
        return RemoteMovieSearchDataSourceImpl(service)
    }

    /**
     * Provide LocalMovieSearchDataSource
     */
    @Provides
    fun provideLocalMovieSearchDataSource(movieDao: MovieDao): LocalMovieSearchDataSource {
        return LocalMovieSearchDataSourceImpl(movieDao)
    }
}
