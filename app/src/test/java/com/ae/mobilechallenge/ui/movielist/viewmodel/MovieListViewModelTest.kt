package com.ae.mobilechallenge.ui.movielist.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.movielist.repository.MovieListRepository
import com.ae.mobilechallenge.util.getOrAwaitValue
import com.ae.mobilechallenge.util.getPopularMoviesMock
import com.ae.mobilechallenge.util.getTopRatedMoviesMock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MovieListViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var repository: MovieListRepository

    @Mock
    private lateinit var observer: Observer<Resource<Pair<List<Movie>, List<Movie>>>>

    private lateinit var viewModel: MovieListViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)
        viewModel = MovieListViewModel(repository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get movies should be success`() {
        testCoroutineDispatcher.runBlockingTest {
            val popularExpect = getPopularMoviesMock()
            val topRatedExpect = getTopRatedMoviesMock()

            BDDMockito.given(repository.getPopularMovies()).willReturn(popularExpect)
            BDDMockito.given(repository.getTopRatedMovies()).willReturn(topRatedExpect)

            viewModel.getMovies().observeForever(observer)
            viewModel.getMovies().getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleSuccess(Pair(popularExpect, topRatedExpect)))
        }
    }

    @ExperimentalCoroutinesApi
    @Test(expected = Exception::class)
    fun `Get movies should be error`() {
        testCoroutineDispatcher.runBlockingTest {
            val expectedException = Exception("")

            BDDMockito.given(repository.getPopularMovies()).willThrow(expectedException)
            BDDMockito.given(repository.getTopRatedMovies()).willThrow(expectedException)

            viewModel.getMovies().observeForever(observer)
            viewModel.getMovies().getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleException(expectedException))
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
