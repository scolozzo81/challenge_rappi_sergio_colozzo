package com.ae.mobilechallenge.ui.movielist.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ae.mobilechallenge.data.datasource.movielist.LocalMovieDataSourceImpl
import com.ae.mobilechallenge.data.datasource.movielist.RemoteMovieDataSourceImpl
import com.ae.mobilechallenge.util.CheckConnection
import com.ae.mobilechallenge.util.getPopularMoviesMock
import com.ae.mobilechallenge.util.getResponseMock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.* // ktlint-disable no-wildcard-imports
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MovieListRepositoryImplTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var localDataSource: LocalMovieDataSourceImpl

    @Mock
    private lateinit var remoteDataSource: RemoteMovieDataSourceImpl

    @Mock
    private lateinit var checkConnection: CheckConnection

    private lateinit var repository: MovieListRepository

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)

        repository = MovieListRepositoryImpl(localDataSource, remoteDataSource, checkConnection)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get popular movies should be success from data source when user has internet`() {
        testCoroutineDispatcher.runBlockingTest {
            val popularExpect = getPopularMoviesMock()
            val responseExpect = getResponseMock(popularExpect)
            BDDMockito.given(remoteDataSource.getPopularMovies()).willReturn(responseExpect)
            BDDMockito.given(localDataSource.getPopularMovies()).willReturn(popularExpect)
            BDDMockito.given(checkConnection.connectionIsAvailable()).willReturn(true)

            val request = repository.getPopularMovies()

            Assert.assertNotNull(popularExpect)
            Assert.assertEquals(popularExpect, request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get popular movies should be success from data source when user hasn't internet`() {
        testCoroutineDispatcher.runBlockingTest {
            val popularExpect = getPopularMoviesMock()
            val responseExpect = getResponseMock(popularExpect)
            BDDMockito.given(remoteDataSource.getPopularMovies()).willReturn(responseExpect)
            BDDMockito.given(localDataSource.getPopularMovies()).willReturn(popularExpect)
            BDDMockito.given(checkConnection.connectionIsAvailable()).willReturn(false)

            val response = repository.getPopularMovies()

            Assert.assertEquals(popularExpect, response)
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
