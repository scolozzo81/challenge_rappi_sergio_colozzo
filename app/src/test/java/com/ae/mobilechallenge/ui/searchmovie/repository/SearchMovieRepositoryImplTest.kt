package com.ae.mobilechallenge.ui.searchmovie.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ae.mobilechallenge.data.datasource.searchmovie.LocalMovieSearchDataSource
import com.ae.mobilechallenge.data.datasource.searchmovie.RemoteMovieSearchDataSource
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.util.getMoviesMock
import com.ae.mobilechallenge.util.getResponseMock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.* // ktlint-disable no-wildcard-imports
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SearchMovieRepositoryImplTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var local: LocalMovieSearchDataSource

    @Mock
    lateinit var remote: RemoteMovieSearchDataSource

    private lateinit var repository: SearchMovieRepository

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)

        repository = SearchMovieRepositoryImpl(local, remote)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get movies by name should be success from service`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = getMoviesMock()
            val responseExpect = getResponseMock(resultExpected)
            val search = "test"
            BDDMockito.given(remote.getTMovieByName(search))
                .willReturn(responseExpect)

            val request = repository.getTMovieByName(search)

            Assert.assertNotNull(request)
            Assert.assertFalse(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get movies by name should be success but not found the movie and return empty list`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = arrayListOf<Movie>()
            val responseExpect = getResponseMock(resultExpected)
            val search = "test"
            BDDMockito.given(remote.getTMovieByName(search))
                .willReturn(responseExpect)

            val request = repository.getTMovieByName(search)

            Assert.assertNotNull(request)
            Assert.assertTrue(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get all movies should be success from DB`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = getMoviesMock()
            BDDMockito.given(local.getAllMovies())
                .willReturn(resultExpected)

            val request = repository.getAllMovies()

            Assert.assertNotNull(request)
            Assert.assertFalse(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get all movies should be success but return empty list from DB`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = arrayListOf<Movie>()
            BDDMockito.given(local.getAllMovies())
                .willReturn(resultExpected)

            val request = repository.getAllMovies()

            Assert.assertNotNull(request)
            Assert.assertTrue(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
