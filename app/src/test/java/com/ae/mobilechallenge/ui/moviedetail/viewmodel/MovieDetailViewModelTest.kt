package com.ae.mobilechallenge.ui.moviedetail.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.MovieResult
import com.ae.mobilechallenge.ui.moviedetail.repository.MovieDetailRepository
import com.ae.mobilechallenge.util.getMovieResultListMock
import com.ae.mobilechallenge.util.getMoviesMock
import com.ae.mobilechallenge.util.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.* // ktlint-disable no-wildcard-imports
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MovieDetailViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var repository: MovieDetailRepository

    @Mock
    private lateinit var observer: Observer<Resource<List<MovieResult>>>

    private lateinit var viewModel: MovieDetailViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)
        viewModel = MovieDetailViewModel(repository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get data of movie to get url Movie should be success`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = getMovieResultListMock()
            val movieId = "1"
            BDDMockito.given(repository.getDataMovie(movieId)).willReturn(resultExpect)

            viewModel.getDataMovie(movieId).observeForever(observer)
            viewModel.getDataMovie(movieId).getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleSuccess(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @Test(expected = Exception::class)
    fun `Get data of movie to get url Movie should be error`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = java.lang.Exception()
            val movieId = "1"
            BDDMockito.given(repository.getDataMovie(movieId)).willThrow(resultExpect)

            viewModel.getDataMovie(movieId).observeForever(observer)
            viewModel.getDataMovie(movieId).getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleException(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
