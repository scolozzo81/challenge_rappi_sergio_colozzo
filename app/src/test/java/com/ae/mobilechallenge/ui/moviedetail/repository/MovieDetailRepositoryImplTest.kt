package com.ae.mobilechallenge.ui.moviedetail.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ae.mobilechallenge.data.datasource.moviedetail.RemoteMovieDetailDataSource
import com.ae.mobilechallenge.dto.MovieResult
import com.ae.mobilechallenge.util.getDataVideoResponseMock
import com.ae.mobilechallenge.util.getMovieResultListMock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.* // ktlint-disable no-wildcard-imports
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MovieDetailRepositoryImplTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var remote: RemoteMovieDetailDataSource

    private lateinit var repository: MovieDetailRepository

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)

        repository = MovieDetailRepositoryImpl(remote)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get data of movie should be success from API`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = getMovieResultListMock()
            val responseExpect = getDataVideoResponseMock(resultExpected)
            val movieId = "1"
            BDDMockito.given(remote.getDataMovie(movieId))
                .willReturn(responseExpect)

            val request = repository.getDataMovie(movieId)

            Assert.assertNotNull(request)
            Assert.assertFalse(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get data of movie should be success but return a empty list from API`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpected = arrayListOf<MovieResult>()
            val responseExpect = getDataVideoResponseMock(resultExpected)
            val movieId = "1"
            BDDMockito.given(remote.getDataMovie(movieId))
                .willReturn(responseExpect)

            val request = repository.getDataMovie(movieId)

            Assert.assertNotNull(request)
            Assert.assertTrue(request.isEmpty())
            Assert.assertEquals(resultExpected, request)
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
