package com.ae.mobilechallenge.ui.searchmovie.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ae.mobilechallenge.common.resource.Resource
import com.ae.mobilechallenge.common.resource.ResponseHandler
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.ui.searchmovie.repository.SearchMovieRepository
import com.ae.mobilechallenge.util.getMoviesMock
import com.ae.mobilechallenge.util.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.* // ktlint-disable no-wildcard-imports
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SearchMovieViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var repository: SearchMovieRepository

    @Mock
    private lateinit var observer: Observer<Resource<List<Movie>>>

    private lateinit var viewModel: SearchMovieViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testCoroutineDispatcher)

        viewModel = SearchMovieViewModel(repository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get movies by name should be success`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = getMoviesMock()
            val search = "test"
            BDDMockito.given(repository.getTMovieByName(search)).willReturn(resultExpect)

            viewModel.getMoviesByName(search).observeForever(observer)
            viewModel.getMoviesByName(search).getOrAwaitValue()

            Mockito.verify(observer).onChanged(Resource.loading(null))
            viewModel.getMoviesByName(search).getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleSuccess(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @Test(expected = Exception::class)
    fun `Get movies by name should be error`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = Exception()
            val search = "test"
            BDDMockito.given(repository.getTMovieByName(search)).willThrow(resultExpect)

            viewModel.getMoviesByName(search).observeForever(observer)
            viewModel.getMoviesByName(search).getOrAwaitValue()

            Mockito.verify(observer).onChanged(Resource.loading(null))
            viewModel.getMoviesByName(search).getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleException(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Get all movies should be success`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = getMoviesMock()
            BDDMockito.given(repository.getAllMovies()).willReturn(resultExpect)

            viewModel.getLastMoviesDownloaded().observeForever(observer)
            viewModel.getLastMoviesDownloaded().getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleSuccess(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @Test(expected = Exception::class)
    fun `Get all movies should be error`() {
        testCoroutineDispatcher.runBlockingTest {
            val resultExpect = Exception()
            BDDMockito.given(repository.getAllMovies()).willThrow(resultExpect)

            viewModel.getLastMoviesDownloaded().observeForever(observer)
            viewModel.getLastMoviesDownloaded().getOrAwaitValue()

            Mockito.verify(observer)
                .onChanged(ResponseHandler.handleException(resultExpect))
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
