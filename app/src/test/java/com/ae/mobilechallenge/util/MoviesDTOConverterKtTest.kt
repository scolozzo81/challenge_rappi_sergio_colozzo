package com.ae.mobilechallenge.util

import com.ae.mobilechallenge.data.local.entity.MovieEntity
import com.ae.mobilechallenge.dto.Movie
import org.junit.Assert.* // ktlint-disable no-wildcard-imports
import org.junit.Test

class MoviesDTOConverterKtTest {

    @Test
    fun `Converter any movie from Movie DTO to Movie entity`() {
        val type = "popular"
        val movie = Movie(id = 1, title = "User Mocked 1", movieType = type)
        val entityExpect = MovieEntity(id = 1, title = "User Mocked 1", movieType = type)
        val result = movie.toMovieEntity(type)

        assertNotNull(result)
        assertEquals(entityExpect, result)
    }

    @Test
    fun `Converter any movie from Movie entity to Movie DTO`() {
        val type = "popular"
        val entity = MovieEntity(id = 1, title = "User Mocked 1", movieType = type)
        val movieExpect = Movie(id = 1, title = "User Mocked 1", movieType = type)
        val result = entity.toMovie()

        assertNotNull(result)
        assertEquals(movieExpect, result)
    }

    @Test
    fun `Converter any list of movies from Movie entity to Movie list`() {
        val entities = getListMoviesEntitiesMock()
        val entityExpect = getMoviesMock()
        val result = entities.toMovie()

        assertNotNull(result)
        assertEquals(entityExpect, result)
    }
}
