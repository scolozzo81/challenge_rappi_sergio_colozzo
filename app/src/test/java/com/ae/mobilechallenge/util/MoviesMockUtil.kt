package com.ae.mobilechallenge.util

import com.ae.mobilechallenge.data.local.entity.MovieEntity
import com.ae.mobilechallenge.dto.DataVideoResponse
import com.ae.mobilechallenge.dto.Movie
import com.ae.mobilechallenge.dto.MovieResult
import com.ae.mobilechallenge.dto.Response

fun getMoviesMock(): List<Movie> {
    val list = arrayListOf<Movie>()
    list.addAll(getPopularMoviesMock())
    list.addAll(getTopRatedMoviesMock())
    return list
}

fun getPopularMoviesMock(): List<Movie> {
    return arrayListOf(
        Movie(id = 1, title = "User Mocked 1", movieType = "popular"),
        Movie(id = 2, title = "User Mocked 2", movieType = "popular")
    )
}

fun getTopRatedMoviesMock(): List<Movie> {
    return arrayListOf(
        Movie(id = 4, title = "User Mocked 4", movieType = "top_rated"),
        Movie(id = 5, title = "User Mocked 5", movieType = "top_rated")
    )
}

fun getListMoviesEntitiesMock(): List<MovieEntity> {
    return arrayListOf(
        MovieEntity(id = 1, title = "User Mocked 1", movieType = "popular"),
        MovieEntity(id = 2, title = "User Mocked 2", movieType = "popular"),
        MovieEntity(id = 4, title = "User Mocked 4", movieType = "top_rated"),
        MovieEntity(id = 5, title = "User Mocked 5", movieType = "top_rated")
    )
}

fun getResponseMock(list: List<Movie>): Response {
    return Response(
        page = 1,
        totalPages = 1,
        movies = list,
        totalResults = 1
    )
}

fun getMovieResultListMock(): List<MovieResult> {
    return arrayListOf(
        MovieResult(id = "1", key = "test", name = "test1", site = "test1"),
        MovieResult(id = "2", key = "test", name = "test2", site = "test2"),
        MovieResult(id = "3", key = "test", name = "test3", site = "test3")
    )
}

fun getDataVideoResponseMock(results: List<MovieResult>): DataVideoResponse {
    return DataVideoResponse(id = 1, results)
}